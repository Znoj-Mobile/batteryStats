package com.example.iri.batterystats;

import android.app.Application;
import android.content.Context;

/**
 * Created by Iri on 16.10.2015.
 */
public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}
