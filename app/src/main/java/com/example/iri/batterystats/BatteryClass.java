package com.example.iri.batterystats;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Iri on 16.10.2015.
 */
public class BatteryClass extends Thread {
    Thread thread;
    boolean bRunning = true;
    private static String stats = "";

    public void run() {
        try {
            bRunning = true;
            while (bRunning) {
                        try {
                            synchronized (this) {
                                wait(3000);
                            }
                        } catch (InterruptedException ex) {
                        }
                Toast.makeText(B_service.getServiceContext(), "text", Toast.LENGTH_SHORT);
                stats += message();
                try {
                    Message msg = Message.obtain();
                    msg.obj = stats;
                    B_service.handler(msg);
                } catch (Exception e) {
                    Log.e("BatteryClass", e.getMessage());
                }
            }
        } catch (Exception e) {
            Log.d("SERVER", "Error");
            e.printStackTrace();
        } finally {
            bRunning = false;
        }
    }

    private String message() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = B_service.getServiceContext().registerReceiver(null, ifilter);

        // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;

        return "status: " + isCharging + ", usbCharge: " + usbCharge + ", acCharge: " + acCharge +
                ", level: " + level + ", scale: " + scale + " → current battery charge: " + batteryPct + "\n";
    }

    public void close(){
        bRunning = false;
    }

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent i) {
            int level = i.getIntExtra("level", 0);
            Log.d("Battery Level: ", Integer.toString(level) + "%");
        }

    };
}
