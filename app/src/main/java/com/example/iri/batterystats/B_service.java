package com.example.iri.batterystats;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Iri on 16.10.2015.
 */
public class B_service extends Service {

    BatteryClass s;
    protected static Service service = null;
    public static boolean activityRunning = false;
    private static Message staticMsg = null;
    private static String stats = "";
    Thread thread;
    private static Context c;

    @Override
    public void onCreate(){
        c = this;
        service = this;
        registerReceiver(mBatInfoReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        startMyService();
    }


    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent i) {
            int level = i.getIntExtra("level", 0);
            Log.d("Battery Level: ", Integer.toString(level) + "%");
            message();
        }

    };

    private String message() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = B_service.getServiceContext().registerReceiver(null, ifilter);

        // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;

        return "status: " + isCharging + ", usbCharge: " + usbCharge + ", acCharge: " + acCharge +
                ", level: " + level + ", scale: " + scale + " → current battery charge: " + batteryPct + "\n";
    }

    public void startMyService(){
        if(s == null) {
            //s = new BatteryClass();
            //s.start();
        }
    }

    public static Context getServiceContext(){
        Toast.makeText(c, "text", Toast.LENGTH_SHORT).show();
        return c;
    }
    static void handler(Message msg){
        staticMsg = msg;
        if(activityRunning){
            MainActivity.handler.sendMessage(msg);
        }
    }

    public void stopMyService(){
        s.close();
    }


    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        B_service getService() {
            // Return this instance of LocalService so clients can call public methods
            return B_service.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }

    //nepouzivam, protoze nevim jak to joinout
    public static void startActivity(){
        Intent i = new Intent();
        i.setClass(service, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        service.startActivity(i);
    }
}
