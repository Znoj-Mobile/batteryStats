package com.example.iri.batterystats2;

import android.app.AlarmManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    SampleAlarmReceiver alarm = new SampleAlarmReceiver();
    CoordinatorLayout cl;
    long repeatTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cl = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        repeatTimer = AlarmManager.INTERVAL_HALF_HOUR;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAlarm();
            }
        });

        class MyListener implements View.OnClickListener
        {
            public MyListener(long interval) {
                repeatTimer = interval;
            }

            @Override
            public void onClick(View v)
            {
                if(((RadioButton)v).isChecked()){
                    startAlarm();
                }
            }
        }
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);
        int id = radioGroup.getCheckedRadioButtonId();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton0){
                    repeatTimer = 10*1000;
                }
                else if(checkedId == R.id.radioButton){
                    repeatTimer = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
                }
                else if(checkedId == R.id.radioButton2){
                    repeatTimer = AlarmManager.INTERVAL_HALF_HOUR;
                }
                else if(checkedId == R.id.radioButton3){
                    repeatTimer = AlarmManager.INTERVAL_HOUR;
                }
                else if(checkedId == R.id.radioButton4){
                    repeatTimer = AlarmManager.INTERVAL_HALF_DAY;
                }
                else if(checkedId == R.id.radioButton5){
                    repeatTimer = AlarmManager.INTERVAL_DAY;
                }
                startAlarm();
            }
        });
        RadioButton rb0 = (RadioButton) findViewById(R.id.radioButton0);
        rb0.setOnClickListener(new MyListener(10*1000));
        RadioButton rb1 = (RadioButton) findViewById(R.id.radioButton);
        rb1.setOnClickListener(new MyListener(AlarmManager.INTERVAL_FIFTEEN_MINUTES));
        RadioButton rb2 = (RadioButton) findViewById(R.id.radioButton2);
        rb2.setOnClickListener(new MyListener(AlarmManager.INTERVAL_HALF_HOUR));
        RadioButton rb3 = (RadioButton) findViewById(R.id.radioButton3);
        rb3.setOnClickListener(new MyListener(AlarmManager.INTERVAL_HOUR));
        RadioButton rb4 = (RadioButton) findViewById(R.id.radioButton4);
        rb4.setOnClickListener(new MyListener(AlarmManager.INTERVAL_HALF_DAY));
        RadioButton rb5 = (RadioButton) findViewById(R.id.radioButton5);
        rb5.setOnClickListener(new MyListener(AlarmManager.INTERVAL_DAY));
    }

    public void startAlarm(){
        alarm.setAlarm(getApplicationContext(), repeatTimer);
        Snackbar.make(cl, R.string.start, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // When the user clicks START ALARM, set the alarm.
            case R.id.start_action:
                startAlarm();
                return true;
            // When the user clicks CANCEL ALARM, cancel the alarm.
            case R.id.cancel_action:
                Snackbar.make(cl, R.string.stop, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                alarm.cancelAlarm(this);
                return true;
        }

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
