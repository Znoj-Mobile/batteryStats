package com.example.iri.batterystats2;

/**
 * Created by Iri on 16.10.2015.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This {@code IntentService} does the app's actual work.
 * {@code SampleAlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class SampleSchedulingService extends IntentService {
    public SampleSchedulingService() {
        super("SchedulingService");
    }

    public static final String TAG = "Scheduling Demo";
    // An ID used to post the notification.
    public static final int NOTIFICATION_ID = 1;
    // The Google home page URL from which the app fetches content.
    // You can find a list of other Google domains with possible doodles here:
    // http://en.wikipedia.org/wiki/List_of_Google_domains


    private static boolean isCharging = false;
    private static boolean usbCharge = false;
    private static boolean acCharge = false;
    private static int level = 0;
    private static int scale = 0;
    private static int battery_result = 0;

    private static String OSVersion = "";
    private static String API = "";
    private static String Device = "";
    private static String Model = "";
    private static String Product = "";
    private String URL = "http://192.168.88.176/batteryStat/setData.php?level=" + level + "&isCharging=" + isCharging + "&usbCharge=" + usbCharge + "&acCharge=" + acCharge +
            "&OSVersion" + OSVersion + "&API=" + API + "&Device=" + Device + "&Model=" + Model + "&Product=" + Product;


    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    @Override
    protected void onHandleIntent(Intent intent) {
        // BEGIN_INCLUDE(service_onhandle)
        // The URL from which to fetch content.

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Context c = this;
        Intent batteryStatus = c.registerReceiver(null, ifilter);
        // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


        level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;

        OSVersion = Build.VERSION.RELEASE;
        API = String.valueOf(android.os.Build.VERSION.SDK_INT);
        Device = android.os.Build.DEVICE;
        Model = android.os.Build.MODEL;
        Product = android.os.Build.PRODUCT;




        String urlString = URL = "http://192.168.88.176/batteryStat/setData.php?level=" + level + "&isCharging=" + isCharging + "&usbCharge=" + usbCharge + "&acCharge=" + acCharge +
                "&OSVersion=" + OSVersion + "&API=" + API + "&Device=" + Device + "&Model=" + Model + "&Product=" + Product;
        URL = URL.replace(" ", "_");
        Log.d("URL", URL);

        sendNotification(message());
        //Log.i(TAG, message());
        String result ="";

        // Try to connect to the Google homepage and download content.
        try {
            result = loadFromNetwork(urlString);
        } catch (IOException e) {
            Log.i(TAG, getString(R.string.connection_error));
        }

        // Release the wake lock provided by the BroadcastReceiver.
        SampleAlarmReceiver.completeWakefulIntent(intent);
        // END_INCLUDE(service_onhandle)
    }

    private String message() {


        String s = "";
/*
        s += "URL:\n";
        s += URL + "\n\n";
*/
        s += "Battery-infos:";
        s += "\nisCharging: " + isCharging;
        if(isCharging) {
            if (usbCharge) {
                s += " (usbCharge)";
            } else if (acCharge) {
                s += " (acCharge)";
            }
        }

        s += "\nlevel: " + level + "% (scale: " + scale + " → " + battery_result + ")\n";

        s += "\nDevice-infos:";
        //s += "\nOS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
        s += "\nOS Version: " + OSVersion + " (API: " + API + ")";
        s += "\nDevice: " + Device;
        s += "\nModel (and Product): " + Model + " ("+ Product + ")";


        return  s;
    }

    // Post a notification indicating whether a doodle was found.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        //.setSmallIcon(R.drawable.ic_launcher)
        //.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher))
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon)
                .setContentTitle(getString(R.string.battery_send_to_server))
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

//
// The methods below this line fetch content from the specified URL and return the
// content as a string.
//
    /** Given a URL string, initiate a fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";

        try {
            stream = downloadUrl(urlString);
            str = readIt(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return str;
    }

    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
    }

    /**
     * Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from www.google.com.
     * @return String version of InputStream.
     * @throws IOException
     */
    private String readIt(InputStream stream) throws IOException {

        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        for(String line = reader.readLine(); line != null; line = reader.readLine())
            builder.append(line);
        reader.close();
        return builder.toString();
    }
}